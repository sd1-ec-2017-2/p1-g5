# Projeto 1 de Sistemas Distribuidos - Grupo 5

## Servidor IRC (Internet Relay Chat) utilizando Sockets em NODEJS

Este projeto tem como objetivo a implementacao de um servidor IRC utilizando sockets em nodejs, ou seja, utilizaremos o protocolo de comunicação IRC para o desenvolvimento de um servidor de bate-papo (chat), permitindo a conversa em grupo ou privada atendendo as normas do [RFC 2812](https://pt.wikipedia.org/wiki/Request_for_Comments) que contém as descrições técnicas do protocolo.

## Integrantes do projeto
* Vagner Luciano da Costa Silva - vagnerbas13@gmail.com - Master e Developer
* Ana Gabriella Frietas Hoffmann - @ah.gabriella - Developer

## Manual de usuário e desenvolvedor

Essas instruções irão auxia-lo a executar o servidor IRC em sua máquina local para fins de desenvolvimento e utilização.

## Dependências de ambiente

### Bibliotecas utilizadas para inicializar o servidor

* Nodejs - interpretador de codigo JavaScript que funciona do lado do servidor. [NODE.JS](https://nodejs.org/en/download/package-manager/).
* Npm - gerênciador de pacotes do nodejs. Não é necessário instalação explicita, pois já é instalado junto com a instalação do nodejs

#### Testando npm e nodejs

Para verificar se o nodejs e o npm estão instalados, pode-se executar os comandos a seguir. Se eles retornarem algum valor diferente de erro então estãó instalados
``` 
     node -v  ou nodejs -v
```
``` 
     npm -v
```


### Clientes IRC para testar o servidor

* [ChatZilla](http://chatzilla.hacksrus.com).
* Telnet

## Instalação e Inicialização do Servidor

 Para instalar o servidor irc é necessario clonar o repositório para a maquina local.
 ```
 	git clone https://gitlab.com/sd1-ec-2017-2/p1-g5/
 ```

**Caso não tenha o git instalado siga as instruções de [GIT - Primeiros Passos](https://git-scm.com/book/pt-br/v1/Primeiros-passos-Instalando-Git)**

 Acesse a pasta do servidor
 
```
	$ cd ~/p1-g5/irc-server
```
 
 Baixe as dependencias do projeto no repositorio npm
 
```
    $ npm install
``` 
 
 Inicialize o servidor
 
```
	$ npm run start
```
 **O servidor IRC será iniciado na porta 6667**

 Para acessar o servidor IRC abra um novo terminal e execute

 ```
 	$ telnet localhost 6667
 ```

Ou utilize a extensão do Mozilla Firefox [ChatZilla](https://addons.mozilla.org/pt-BR/firefox/addon/chatzilla/)

### Testes unitários e verificação de código

Caso deseje rodar os testes unitários deve-se executar o comando
```
	$ npm run test
```

Também é possível realizar um lint do projeto, para verificar se existe código fora dos padrões definidos para linguagem
```
	$ npm run lint
```

## Manuais

[Lista de Mensagens Suportadas](/documentacao/lista-mensagens-suportadas.md)

[Caso de Teste](/documentacao/casosdeteste.md)
  

## Referência
Você pode se interessar também em olhar outros servidores IRC disponíveis no Repositório do Ubuntu. Isto inclui ircd-ircu e ircd-hybrid.
	Visite [IRCD FAQ](http://www.irc.org/tech_docs/ircnet/faq.html) para mais detalhes sobre o servidor IRC.

Para maiores informações sobre as normas do protocolo utilizado para o desenvolvimento deste servidor visite [RFC 2812 - Internet Relay Chat](https://tools.ietf.org/html/rfc2812).
